#!/bin/bash
set -e

./gradlew build -x test

echo
echo "Starting..."
echo

exec "$@"
