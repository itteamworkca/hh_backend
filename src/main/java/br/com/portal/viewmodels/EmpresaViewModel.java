package br.com.portal.viewmodels;

import javax.validation.Valid;

import br.com.portal.entities.shared.Empresa;
import br.com.portal.entities.shared.Solucao;

public class EmpresaViewModel {
    private Empresa empresa;
    private Solucao produto;

    @Valid
    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    @Valid
    public Solucao getProduto() {
        return produto;
    }

    public void setProduto(Solucao produto) {
        this.produto = produto;
    }
}
