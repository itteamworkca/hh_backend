package br.com.portal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import br.com.portal.configs.oauth2.CustomUser;
import br.com.portal.entities.shared.Filtro;
import br.com.portal.services.FiltroService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/filtro")
@CrossOrigin
public class FiltroController {

    @Autowired
    private FiltroService filtroService;

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(method = RequestMethod.GET)
    public List<Filtro> list(@AuthenticationPrincipal CustomUser principal) {
        return filtroService.findAll();
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Filtro> create(@Valid @RequestBody Filtro filtro, @AuthenticationPrincipal CustomUser principal) {
        Filtro entity = filtroService.create(filtro);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Filtro> retrieve(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Filtro entity = filtroService.find(id);
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

//	@PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
//    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
//    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Filtro current, @AuthenticationPrincipal CustomUser principal) {
//		Filtro original = filtroService.find(id);
//        if (original == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        Filtro entity = filtroService.update(original, current);
//        return new ResponseEntity<>(entity, HttpStatus.OK);
//    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Filtro entity = filtroService.find(id);
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        filtroService.delete(entity);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
