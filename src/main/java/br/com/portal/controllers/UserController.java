package br.com.portal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import br.com.portal.configs.oauth2.CustomUser;
import br.com.portal.entities.shared.User;
import br.com.portal.services.UserService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(method = RequestMethod.GET)
    public List<User> list(@AuthenticationPrincipal CustomUser principal) {
        return userService.findAll();
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> create(@Valid @RequestBody User user, @AuthenticationPrincipal CustomUser principal) {
        User entity = userService.create(user);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable("id") UUID id, @RequestBody User current, @AuthenticationPrincipal CustomUser principal) {
        User original = userService.find(id);
        if (original == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        User entity = userService.update(original, current);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") UUID id, @AuthenticationPrincipal CustomUser principal) {
        User entity = userService.find(id);
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        userService.delete(entity);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
