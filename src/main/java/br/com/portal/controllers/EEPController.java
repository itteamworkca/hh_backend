package br.com.portal.controllers;

import org.apache.tika.mime.MimeTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import br.com.portal.entities.shared.*;
import br.com.portal.services.*;
import br.com.portal.viewmodels.EmpresaViewModel;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/eep")
@CrossOrigin
public class EEPController {

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private ImagemService imagemService;

    @Autowired
    private FiltroService filtroService;

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "/tags", method = RequestMethod.GET)
    public ResponseEntity tagSearch(@RequestParam(value = "nome", required = true) String search) {
        List<Tag> tags = tagService.find(search);

        return new ResponseEntity<>(tags, HttpStatus.OK);
    }

    @RequestMapping(value = "/filtros", method = RequestMethod.GET)
    public ResponseEntity filtros() {
        List<Filtro> filtros = filtroService.findAll();
        return new ResponseEntity<>(filtros, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@Valid @RequestBody EmpresaViewModel viewModel) {
        Empresa entity = empresaService.create(viewModel);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity upload(@RequestParam("file") MultipartFile file) throws IOException, MimeTypeException {
        if (file.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Imagem imagem = imagemService.create(file);

        if (imagem == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(imagem, HttpStatus.OK);
    }
}
