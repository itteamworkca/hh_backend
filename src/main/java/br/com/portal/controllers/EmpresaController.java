package br.com.portal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import br.com.portal.configs.oauth2.CustomUser;
import br.com.portal.entities.shared.Empresa;
import br.com.portal.services.EmpresaService;
import br.com.portal.viewmodels.EmpresaViewModel;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/empresa")
@CrossOrigin
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    //    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(method = RequestMethod.GET)
    public List<Empresa> list() {
        return empresaService.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@Valid @RequestBody EmpresaViewModel viewModel) {
        Empresa entity = empresaService.create(viewModel);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity retrieve(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Empresa entity = empresaService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Empresa current, @AuthenticationPrincipal CustomUser principal) {
        Empresa original = empresaService.find(id);

        if (original == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Empresa entity = empresaService.update(original, current);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Empresa entity = empresaService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        empresaService.delete(entity);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
