package br.com.portal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.com.portal.entities.shared.Instituicao;
import br.com.portal.services.InstituicaoService;

import javax.validation.Valid;

@RestController
@RequestMapping("/signup")
@CrossOrigin
public class SignupController {

    @Autowired
    private InstituicaoService instituicaoService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@Valid @RequestBody Instituicao instituicao) {
        Instituicao entity = instituicaoService.create(instituicao);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
