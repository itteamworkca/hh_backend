package br.com.portal.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.portal.configs.oauth2.CustomUser;
import br.com.portal.entities.shared.Avaliacao;
import br.com.portal.entities.shared.Comentario;
import br.com.portal.entities.shared.ResultadosBusca;
import br.com.portal.entities.shared.Solucao;
import br.com.portal.services.AvaliacaoService;
import br.com.portal.services.ComentarioService;
import br.com.portal.services.SolucaoService;

@RestController
@RequestMapping("/solucao")
@CrossOrigin
public class SolucaoController {

    @Autowired
    private ComentarioService comentarioService;
    
    @Autowired
    private SolucaoService solucaoService;
    
    @Autowired
    private AvaliacaoService avaliacaoService;

    
    /**COMENTARIO*/
    
    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/comentario", method = RequestMethod.GET)
    public List<Comentario> list(@AuthenticationPrincipal CustomUser principal) {
        return comentarioService.findAll();
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/comentario", method = RequestMethod.POST)
    public ResponseEntity<Comentario> create(@Valid @RequestBody Comentario comentario, @AuthenticationPrincipal CustomUser principal) {
        Comentario entity = comentarioService.create(comentario);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/comentario/{id}", method = RequestMethod.GET)
    public ResponseEntity<Comentario> retrieve(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Comentario entity = comentarioService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/comentario/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateCommentario(@PathVariable("id") Long id, @RequestBody Comentario current, @AuthenticationPrincipal CustomUser principal) {
        Comentario original = comentarioService.find(id);
        if (original == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Comentario entity = comentarioService.update(original, current);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/comentario/{id}", method = RequestMethod.DELETE)
    public ResponseEntity removeComentario(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Comentario entity = comentarioService.find(id);
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        comentarioService.delete(entity);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
    
    
    /**BUSCA DA SOLUCAO*/
    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/busca", method = RequestMethod.GET)
    public List<ResultadosBusca> buscaSolucoes(@RequestParam(value = "nome", required = true) String search) {
        return solucaoService.searchSolucoes(search);
    }
    
    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/busca/{id}", method = RequestMethod.GET)
    public Solucao buscaSolucaoById(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        return solucaoService.findOne(id);
    }
    
    
    /**AVALICAO*/
    
    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/avaliacao", method = RequestMethod.POST)
    public ResponseEntity<Avaliacao> create(@Valid @RequestBody Avaliacao avaliacao, @AuthenticationPrincipal CustomUser principal) {
        Avaliacao entity = avaliacaoService.create(avaliacao);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
