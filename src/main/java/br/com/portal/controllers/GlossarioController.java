package br.com.portal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import br.com.portal.configs.oauth2.CustomUser;
import br.com.portal.entities.shared.Glossario;
import br.com.portal.services.GlossarioService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/glossario")
@CrossOrigin
public class GlossarioController {

    @Autowired
    private GlossarioService glossarioService;

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(method = RequestMethod.GET)
    public List<Glossario> list(@AuthenticationPrincipal CustomUser principal) {
        return glossarioService.findAll();
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@Valid @RequestBody Glossario glossary, @AuthenticationPrincipal CustomUser principal) {
        Glossario entity = glossarioService.create(glossary);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity retrieve(@PathVariable("id") UUID id, @AuthenticationPrincipal CustomUser principal) {
        Glossario entity = glossarioService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable("id") UUID id, @RequestBody Glossario current, @AuthenticationPrincipal CustomUser principal) {
        Glossario original = glossarioService.find(id);

        if (original == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Glossario entity = glossarioService.update(original, current);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") UUID id, @AuthenticationPrincipal CustomUser principal) {
        Glossario entity = glossarioService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        glossarioService.delete(entity);

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
