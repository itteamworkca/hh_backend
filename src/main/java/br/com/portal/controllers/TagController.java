package br.com.portal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import br.com.portal.configs.oauth2.CustomUser;
import br.com.portal.entities.shared.Tag;
import br.com.portal.services.TagService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tag")
@CrossOrigin
public class TagController {

    @Autowired
    private TagService tagService;

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(method = RequestMethod.GET)
    public List<Tag> list(@AuthenticationPrincipal CustomUser principal) {
        return tagService.findAll();
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/getTags/{id}", method = RequestMethod.GET)
    public List<Tag> getTagsFromCategoria(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        return tagService.findTagsFromCategory(id);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Tag> create(@Valid @RequestBody Tag tag, @AuthenticationPrincipal CustomUser principal) {
        Tag entity = tagService.create(tag);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Tag> retrieve(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Tag entity = tagService.find(id);

        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Tag current, @AuthenticationPrincipal CustomUser principal) {
        Tag original = tagService.find(id);
        if (original == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Tag entity = tagService.update(original, current);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('ADMINISTRATOR, INSTITUTION, USER')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity remove(@PathVariable("id") Long id, @AuthenticationPrincipal CustomUser principal) {
        Tag entity = tagService.find(id);
        if (entity == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        tagService.delete(entity);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }
}
