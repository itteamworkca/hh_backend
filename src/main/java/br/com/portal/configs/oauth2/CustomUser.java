package br.com.portal.configs.oauth2;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import br.com.portal.data.Role;
import br.com.portal.entities.shared.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CustomUser extends org.springframework.security.core.userdetails.User {

    private static final long serialVersionUID = 1L;
    private final User decorated;

    public CustomUser(User decorated) {
        super(decorated.getLogin().getEmail(),
            decorated.getLogin().getPassword(),
            decorated.getLogin().getAccountEnabled(),
            decorated.getLogin().getAccountNonExpired(),
            decorated.getLogin().getCredentialsNonExpired(),
            decorated.getLogin().getAccountNonLocked(),
            getAuthorities(decorated.getLogin().getRoles())
        );

        this.decorated = decorated;
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(List<Role> papeis) {
        List<GrantedAuthority> roles = new ArrayList<>();

        for (Role role : papeis) {
            roles.add(new SimpleGrantedAuthority(role.toString()));
        }

        return roles;
    }

    public User getUser() {
        return decorated;
    }
}
