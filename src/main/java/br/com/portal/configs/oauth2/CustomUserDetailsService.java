package br.com.portal.configs.oauth2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import br.com.portal.entities.shared.User;
import br.com.portal.services.UserService;

@Configuration
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.findOneByEmail(email);

        if (user == null) {
            LOGGER.info("Email {} not found", email);
            throw new UsernameNotFoundException("Email not found");
        }

        LOGGER.info("Login: {}", user.getLogin().getEmail());

        return new CustomUser(user);
    }
}
