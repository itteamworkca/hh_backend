package br.com.portal.configs.datasources;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "br.com.portal.repositories", entityManagerFactoryRef = "sharedEntityManagerFactory", transactionManagerRef = "sharedTransactionManager")
public class SharedDataSourceConfig extends AbstractDataSourceConfig {

    public static final String PERSISTENCE_UNIT = "shared";

    @Bean(name = "sharedDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource sharedDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "sharedEntityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean sharedEntityManagerFactory(EntityManagerFactoryBuilder builder) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "create-drop");

        return builder
            .dataSource(sharedDataSource())
            .packages("br.com.portal.entities.shared")
            .persistenceUnit(PERSISTENCE_UNIT)
            .properties(getHibernateProperties())
            .build();
    }

    @Bean(name = "sharedTransactionManager")
    @Primary
    public PlatformTransactionManager sharedTransactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setDataSource(sharedDataSource());
        jpaTransactionManager.setPersistenceUnitName(PERSISTENCE_UNIT);
        return jpaTransactionManager;
    }
}
