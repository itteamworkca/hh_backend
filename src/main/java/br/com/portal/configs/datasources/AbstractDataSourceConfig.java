package br.com.portal.configs.datasources;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public abstract class AbstractDataSourceConfig {

    @Value("${spring.jpa.properties.hibernate.dialect}")
    private String hibernateDialect;

    @Value("${spring.jpa.hibernate.naming-strategy}")
    private String hibernateNamingStrategy;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String hibernateDDLAuto;

    @Value("${spring.jpa.hibernate.generate_statistics}")
    private String hibernateGenerateStatistics;

    @Value("${spring.jpa.hibernate.connection.CharSet}")
    private String hibernateConnectionCharSet;

    @Value("${spring.jpa.hibernate.connection.characterEncoding}")
    private String hibernateConnectionCharacterEncoding;

    @Value("${spring.jpa.hibernate.connection.useUnicode}")
    private String hibernateConnectionUseUnicode;

    @Value("${spring.jpa.hibernate.enable_lazy_load_no_trans}")
    private String hibernateEnableLazyLoadNoTrans;

    @Value("${spring.jpa.show-sql}")
    private String hibernateShowSQL;

    protected Map<String, Object> getHibernateProperties() {
        Map<String, Object> properties = new HashMap<>();

        properties.put("hibernate.dialect", hibernateDialect);
        properties.put("hibernate.naming_strategy", hibernateNamingStrategy); // FIX
        properties.put("hibernate.hbm2ddl.auto", hibernateDDLAuto);
        properties.put("hibernate.generate_statistics", hibernateGenerateStatistics);
        properties.put("hibernate.connection.CharSet", hibernateConnectionCharSet);
        properties.put("hibernate.connection.characterEncoding", hibernateConnectionCharacterEncoding);
        properties.put("hibernate.connection.useUnicode", hibernateConnectionUseUnicode);
        properties.put("hibernate.enable_lazy_load_no_trans", hibernateEnableLazyLoadNoTrans);
        properties.put("hibernate.show-sql", hibernateShowSQL);

        return properties;
    }
}
