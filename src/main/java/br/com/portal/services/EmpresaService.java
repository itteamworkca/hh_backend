package br.com.portal.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.portal.entities.shared.Empresa;
import br.com.portal.entities.shared.Solucao;
import br.com.portal.entities.shared.SolucaoTag;
import br.com.portal.repositories.EmpresaRepository;
import br.com.portal.viewmodels.EmpresaViewModel;

@Service
@Transactional
public class EmpresaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmpresaService.class);

    @Autowired
    private EmpresaRepository empresaRepository;

    public List<Empresa> findAll() {
        LOGGER.info("Find all empresas");
        List<Empresa> list = empresaRepository.findAllByActiveTrue();
        if (list == null) {
            LOGGER.info("Empresa é nulo");
            return null;
        }
        return list;
    }

    public Empresa find(Long id) {
        LOGGER.info("Find Empresa - {}", id);
        Empresa model = empresaRepository.findOneByIdAndActiveTrue(id);
        if (model == null) {
            LOGGER.info("Empresa {} not found", id);
            return null;
        }
        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public Empresa create(EmpresaViewModel viewModel) {
        Empresa empresa = viewModel.getEmpresa();
        Solucao solucao = viewModel.getProduto();
        solucao.setEmpresa(empresa);
        empresa.getSolucoes().add(solucao);

        return create(empresa);
    }

    public Empresa create(Empresa model) {
        LOGGER.info("Creating Empresa");

        setSolucaoInSolucaoTags(model);

        empresaRepository.save(model);
        LOGGER.debug("Created: {}", model);
        return model;
    }

    public Empresa update(Empresa original, Empresa current) {
        LOGGER.info("Updating Empresa - {}", original.getId());
        merge(original, current);
        empresaRepository.save(original);
        LOGGER.debug("Updated: {}", original);
        return original;
    }

    public Empresa delete(Empresa model) {
        LOGGER.info("Removing Empresa - {}", model.getId());
        model.setActive(false);
        empresaRepository.save(model);
        LOGGER.debug("Empresa: {}", model);
        return model;
    }

    private void merge(Empresa original, Empresa current) {
        if (!StringUtils.isEmpty(current.getNome())) {
            original.setNome(current.getNome());
        }

        if (!StringUtils.isEmpty(current.getLogo())) {
            original.setLogo(current.getLogo());
        }

        if (!StringUtils.isEmpty(current.getTelefone())) {
            original.setTelefone(current.getTelefone());
        }

        if (!StringUtils.isEmpty(current.getEmail())) {
            original.setEmail(current.getEmail());
        }

        if (!StringUtils.isEmpty(current.getSite())) {
            original.setSite(current.getSite());
        }

        if (!StringUtils.isEmpty(current.getDataFundacao())) {
            original.setDataFundacao(current.getDataFundacao());
        }

        if (!StringUtils.isEmpty(current.getResponsavelId())) {
            original.setResponsavelId(current.getResponsavelId());
        }

        if (!StringUtils.isEmpty(current.getLocalAtuacao())) {
            original.setLocalAtuacao(current.getLocalAtuacao());
        }

        if (!StringUtils.isEmpty(current.getEndereco())) {
            original.setEndereco(current.getEndereco());
        }

        if (!StringUtils.isEmpty(current.getDescricao())) {
            original.setDescricao(current.getDescricao());
        }

        if (!StringUtils.isEmpty(current.isVendeuParaGoverno())) {
            original.setVendeuParaGoverno(current.isVendeuParaGoverno());
        }

        if (!StringUtils.isEmpty(current.getPrincipaisClientes())) {
            original.setPrincipaisClientes(current.getPrincipaisClientes());
        }

        if (!StringUtils.isEmpty(current.getFaixaFaturamento())) {
            original.setFaixaFaturamento(current.getFaixaFaturamento());
        }

        if (!StringUtils.isEmpty(current.getFuncionariosTempoIntegral())) {
            original.setFuncionariosTempoIntegral(current.getFuncionariosTempoIntegral());
        }

        if (!StringUtils.isEmpty(current.getGestores())) {
            original.setGestores(current.getGestores());
        }

        if (!StringUtils.isEmpty(current.isAceleracaoOuIncubadora())) {
            original.setAceleracaoOuIncubadora(current.isAceleracaoOuIncubadora());
        }

        if (!StringUtils.isEmpty(current.getValorInvestimento())) {
            original.setValorInvestimento(current.getValorInvestimento());
        }

        if (!StringUtils.isEmpty(current.getTipoInvestimento())) {
            original.setTipoInvestimento(current.getTipoInvestimento());
        }

        if (!StringUtils.isEmpty(current.getNomeAceleradora())) {
            original.setNomeAceleradora(current.getNomeAceleradora());
        }

        if (!StringUtils.isEmpty(current.isBuscaInvestimento())) {
            original.setBuscaInvestimento(current.isBuscaInvestimento());
        }
    }

    private void setSolucaoInSolucaoTags(Empresa model) {
        for (Solucao solucao : model.getSolucoes()) {
            if (!solucao.getSolucaoTags().isEmpty()) {
                int position = 0;

                for (SolucaoTag solucaoTag : solucao.getSolucaoTags()) {
                    if (solucaoTag.getPk().getSolucao() == null) {
                        solucaoTag.getPk().setSolucao(solucao);
                        solucaoTag.setPosicionamento(position++);
                    }
                }
            }
        }
    }
}
