package br.com.portal.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.portal.entities.shared.Glossario;
import br.com.portal.repositories.GlossarioRepository;

import java.util.List;
import java.util.UUID;

@Service
public class GlossarioService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlossarioService.class);

    @Autowired
    private GlossarioRepository glossarioRepository;

    public List<Glossario> findAll() {
        LOGGER.info("Find all glossarios");
        List<Glossario> list = glossarioRepository.findAll();
        if (list == null) {
            LOGGER.info("Glossario é nulo");
            return null;
        }
        return list;
    }

    public Glossario find(UUID id) {
        LOGGER.info("Find Glossary - {}", id);

        Glossario model = glossarioRepository.findOneByIdAndActiveTrue(id);

        if (model == null) {
            LOGGER.info("Glossary {} not found", id);
            return null;
        }

        LOGGER.debug("Retrieved: {}", model);

        return model;
    }

    public Glossario create(Glossario model) {
        LOGGER.info("Creating Glossary");

        glossarioRepository.save(model);

        LOGGER.debug("Created: {}", model);

        return model;
    }

    public Glossario update(Glossario original, Glossario current) {
        LOGGER.info("Updating Glossary - {}", original.getId());

        merge(original, current);
        glossarioRepository.save(original);

        LOGGER.debug("Updated: {}", original);

        return original;
    }

    public Glossario delete(Glossario model) {
        LOGGER.info("Removing Glossary - {}", model.getId());

        model.setActive(false);
        glossarioRepository.save(model);

        LOGGER.debug("Glossary: {}", model);

        return model;
    }

    private void merge(Glossario original, Glossario current) {
        if (!StringUtils.isEmpty(current.getTitulo())) {
            original.setTitulo(current.getTitulo());
        }

        if (!StringUtils.isEmpty(current.getDescricao())) {
            original.setDescricao(current.getDescricao());
        }

        if (!StringUtils.isEmpty(current.getImagem())) {
            original.setImagem(current.getImagem());
        }
    }
}
