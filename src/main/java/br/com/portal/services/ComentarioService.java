package br.com.portal.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.portal.entities.shared.Comentario;
import br.com.portal.repositories.ComentarioRepository;

@Service
public class ComentarioService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComentarioService.class);

    @Autowired
    private ComentarioRepository comentarioRepository;

    public List<Comentario> findAll() {
    	LOGGER.info("Find all comentarios");
    	List<Comentario> list = comentarioRepository.findAll();
    	if(list == null){
    		LOGGER.info("Comentario é nulo");
    		return null;
    	}
        return list;
    }

    public Comentario find(Long id) {
        LOGGER.info("Find Comentario - {}", id);
        Comentario model = comentarioRepository.findOne(id);
        if (model == null) {
            LOGGER.info("Comentario {} not found", id);
            return null;
        }
        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public Comentario create(Comentario model) {
        LOGGER.info("Creating Comentario");
        comentarioRepository.save(model);
        LOGGER.debug("Created: {}", model);
        return model;
    }

    public Comentario update(Comentario original, Comentario current) {
        LOGGER.info("Updating Filtro - {}", original.getId());
        merge(original, current);
        comentarioRepository.save(original);
        LOGGER.debug("Updated: {}", original);
        return original;
    }

    public Comentario delete(Comentario model) {
        LOGGER.info("Removing Comentario - {}", model.getId());
        comentarioRepository.delete(model);
        LOGGER.debug("Comentario: {}", model);
        return model;
    }

    private void merge(Comentario original, Comentario current) {
        if (!StringUtils.isEmpty(current.getComentario())) {
            original.setComentario(current.getComentario());
        }

        if (!StringUtils.isEmpty(current.getSolucao())) {
            original.setSolucao(current.getSolucao());
        }
    }

}
