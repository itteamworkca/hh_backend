package br.com.portal.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.com.portal.components.UploadComponent;
import br.com.portal.entities.shared.Imagem;
import br.com.portal.repositories.ImagemRepository;

@Service
@Transactional
public class ImagemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImagemService.class);

    @Autowired
    private UploadComponent uploadComponent;

    @Autowired
    private ImagemRepository imagemRepository;

    public Imagem create(MultipartFile file) {
        LOGGER.info("Creating Imagem");

        String filename = uploadComponent.store(file);
        Imagem model = assembly(filename);

        imagemRepository.save(model);

        LOGGER.debug("Created: {}", model.getImagem());
        return model;
    }

    private Imagem assembly(String filename) {
        Imagem imagem = new Imagem();
        imagem.setImagem(filename);

        return imagem;
    }
}
