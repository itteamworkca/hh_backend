package br.com.portal.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.portal.entities.shared.ResultadosBusca;
import br.com.portal.entities.shared.Solucao;
import br.com.portal.entities.shared.Tag;
import br.com.portal.repositories.SolucaoRepository;

@Service
@Transactional
public class SolucaoService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SolucaoService.class);
	@Autowired
	private SolucaoRepository solucaoRepository;

	public List<Solucao> findAll(){
		LOGGER.info("Find all solucoes");
		List<Solucao> list = solucaoRepository.findAllByActiveTrue();
		if(list == null){
			LOGGER.info("solucao eh nula");
			return null;
		}
		return list;
	}

	public Solucao findOne(Long id){
		LOGGER.info("Find one solucao");
		Solucao model = solucaoRepository.findOne(id);
		if(model == null){
			LOGGER.info("solucao not found");
			return null;
		}
		LOGGER.info("Solucao { } found");
		return model;
	}

	public List<ResultadosBusca> searchSolucoes(String search){
		LOGGER.info("Find solucoes with string: " +search);
		List<ResultadosBusca> list = new ArrayList<ResultadosBusca>();
		
		/**Busca na solucao*/
		List<Solucao> solucaoList = solucaoRepository.buscaFreeTextBySolucao(search);
		//TODO: buscar pelas tags, nome de empresa
		List<Tag> tagsList = solucaoRepository.buscaByTagFreeText(search);
		
		List<Solucao> solucaoList2 = solucaoRepository.buscaSolucaoByTagDescricaoFreeText(search);
		
		for(Solucao s : solucaoList){
			ResultadosBusca rb = new ResultadosBusca();
			rb.setId(s.getId());
			rb.setTitulo(s.getNome());
			rb.setSubtitulo(s.getSite());
			rb.setDescricao(s.getResumo());

			if(s.getImagens() != null && s.getImagens().size() > 0){
				rb.setImagem(s.getImagens().toArray()[0].toString());
			}

			if(s.getSolucaoTags() != null && s.getSolucaoTags().size() > 0){
				ArrayList<Tag> tags = new ArrayList<Tag>();
				Tag tag = new Tag();
				tag.setNome(s.getNome());
				tag.setSolucaoTags(s.getSolucaoTags());
				tag.setActive(true);
				tags.add(tag);
				rb.setTags(tags);
			}
			rb.setData((java.sql.Date) s.getDataCadastro());
			rb.setEmpresa(s.getEmpresa().getNome());
			rb.setPrincipalUsuario(s.getPrincipalUsuario().toString());
			list.add(rb);

		}
		return list;
	}

}
