package br.com.portal.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.portal.entities.shared.Avaliacao;
import br.com.portal.repositories.AvaliacaoRepository;

@Service
public class AvaliacaoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AvaliacaoService.class);

    @Autowired
    private AvaliacaoRepository avaliacaoRepository;

    public List<Avaliacao> findAll() {
    	LOGGER.info("Find all avaliacao");
    	List<Avaliacao> list = avaliacaoRepository.findAll();
    	if(list == null){
    		LOGGER.info("Avaliacao é nulo");
    		return null;
    	}
        return list;
    }


    public Avaliacao create(Avaliacao model) {
        LOGGER.info("Creating Avaliacao");
        avaliacaoRepository.save(model);
        LOGGER.debug("Created: {}", model);
        return model;
    }

    

}
