package br.com.portal.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.portal.components.SecurityComponent;
import br.com.portal.entities.shared.Endereco;
import br.com.portal.entities.shared.Instituicao;
import br.com.portal.entities.shared.Login;
import br.com.portal.repositories.InstituicaoRepository;

import javax.validation.ValidationException;
import java.util.List;
import java.util.UUID;

@Transactional
@Service
public class InstituicaoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InstituicaoService.class);

    @Autowired
    private SecurityComponent securityComponent;

    @Autowired
    private InstituicaoRepository instituicaoRepository;

    public Instituicao find(UUID id) {
        LOGGER.info("Find Instituicao - {}", id);

        Instituicao model = instituicaoRepository.findOneByIdAndActiveTrue(id);

        if (model == null) {
            LOGGER.info("Instituicao {} not found", id);
            return null;
        }

        LOGGER.debug("Retrieved: {}", model);

        return model;
    }

    public Instituicao create(Instituicao model) {
        LOGGER.info("Creating Instituicao");

        checkDuplicatedEmail(model);

        hashPassword(model.getLogin());
        instituicaoRepository.save(model);

        LOGGER.debug("Created: {}", model);

        return model;
    }

    public Instituicao update(Instituicao original, Instituicao current) {
        LOGGER.info("Updating Instituicao - {}", original.getId());

        merge(original, current);
        instituicaoRepository.save(original);

        LOGGER.debug("Updated: {}", original);

        return original;
    }

    public Instituicao delete(Instituicao model) {
        LOGGER.info("Removing Instituicao - {}", model.getId());

        model.setActive(false);
        instituicaoRepository.save(model);

        LOGGER.debug("Instituicao: {}", model);

        return model;
    }

    private void hashPassword(Login model, String salt) {
        String password = model.getPassword();
        String[] hashedPassword = securityComponent.hashPasswordWithBcrypt(password, salt);

        model.setPassword(hashedPassword[0]);
        model.setSalt(hashedPassword[1]);
    }

    private void hashPassword(Login model) {
        hashPassword(model, null);
    }

    private void checkDuplicatedEmail(Instituicao model) {
        List<Instituicao> instituicoes = instituicaoRepository.findAllByLoginEmailAndActiveTrue(model.getLogin().getEmail());

        if (!instituicoes.isEmpty()) {
            // TODO
            LOGGER.info(" > Duplicated email : {}", model.getLogin().getEmail());
            throw new ValidationException();
        }
    }

    private void checkDuplicatedEmail(Instituicao model, UUID id) {
        List<Instituicao> instituicoes = instituicaoRepository.findAllByLoginEmailAndIdNotAndActiveTrue(model.getLogin().getEmail(), id);

        if (!instituicoes.isEmpty()) {
            // TODO
            LOGGER.info(" > Duplicated email : {}", model.getLogin().getEmail());
            throw new ValidationException();
        }
    }

    private void merge(Instituicao original, Instituicao current) {
        if (!StringUtils.isEmpty(current.getNome())) {
            original.setNome(current.getNome());
        }

        // Login
        merge(current.getLogin(), original.getLogin(), current, original.getId());

        // Address
        merge(original.getEndereco(), current.getEndereco());
    }

    private void merge(Login current, Login original, Instituicao model, UUID id) {
        if (!StringUtils.isEmpty(current.getEmail())) {
            checkDuplicatedEmail(model, id);
            original.setEmail(current.getEmail());
        }

        if (!StringUtils.isEmpty(current.getPassword())) {
            hashPassword(current, original.getSalt());
            original.setPassword(current.getPassword());
        }
    }

    private void merge(Endereco original, Endereco current) {
        if (!StringUtils.isEmpty(current.getCep())) {
            original.setCep(current.getCep());
        }

        if (!StringUtils.isEmpty(current.getEndereco())) {
            original.setEndereco(current.getEndereco());
        }

        if (!StringUtils.isEmpty(current.getComplemento())) {
            original.setComplemento(current.getComplemento());
        }

        if (!StringUtils.isEmpty(current.getCidade())) {
            original.setCidade(current.getCidade());
        }

        if (!StringUtils.isEmpty(current.getEstado())) {
            original.setEstado(current.getEstado());
        }
        if (!StringUtils.isEmpty(current.getBairro())) {
            original.setBairro(current.getBairro());
        }
        if (!StringUtils.isEmpty(current.getPais())) {
            original.setPais(current.getPais());
        }
    }
}
