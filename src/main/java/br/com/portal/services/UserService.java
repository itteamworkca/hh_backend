package br.com.portal.services;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import br.com.portal.entities.shared.User;
import br.com.portal.repositories.UserRepository;

@Service
@Transactional
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public User findOneByEmail(String email) {
        LOGGER.info("Find User by Email: {}", email);
        User model = userRepository.findOneByLoginEmailAndLoginAccountEnabledTrueAndActiveTrue(email);
        if (model == null) {
            LOGGER.info("User {} not found", email);
            return null;
        }
        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public List<User> findAll() {
        LOGGER.info("Find all users");
        List<User> list = userRepository.findAll();
        if (list == null) {
            LOGGER.info("User é nulo");
            return null;
        }
        return list;
    }

    public User find(UUID id) {
        LOGGER.info("Find user - {}", id);
        User model = userRepository.findOneByIdAndActiveTrue(id);
        if (model == null) {
            LOGGER.info("User {} not found", id);
            return null;
        }
        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public User create(User model) {
        LOGGER.info("Creating User");
        userRepository.save(model);
        LOGGER.debug("Created: {}", model);
        return model;
    }

    public User update(User original, User current) {
        LOGGER.info("Updating user - {}", original.getId());
        merge(original, current);
        userRepository.save(original);
        LOGGER.debug("Updated: {}", original);
        return original;
    }

    public User delete(User model) {
        LOGGER.info("Removing user - {}", model.getId());
        model.setActive(false);
        userRepository.save(model);
        LOGGER.debug("User: {}", model);
        return model;
    }

    private void merge(User original, User current) {
        if (!StringUtils.isEmpty(current.getLogin())) {
            original.setLogin(current.getLogin());
        }
        if (!StringUtils.isEmpty(current.getAreaAtuacao())) {
            original.setAreaAtuacao(current.getAreaAtuacao());
        }
        if (!StringUtils.isEmpty(current.getEmpresa())) {
            original.setEmpresa(current.getEmpresa());
        }
        if (!StringUtils.isEmpty(current.getEspecialidade())) {
            original.setEspecialidade(current.getEspecialidade());
        }
    }
}
