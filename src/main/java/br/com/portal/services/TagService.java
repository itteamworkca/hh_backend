package br.com.portal.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.portal.entities.shared.Tag;
import br.com.portal.repositories.TagRepository;

import java.text.Normalizer;
import java.util.List;

@Service
public class TagService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TagService.class);

    @Autowired
    private TagRepository tagRepository;

    public List<Tag> find(String search) {
        LOGGER.info("Searching for tag: {}", search);

        String containsSearch = normalizeForContainsSearch(search);

        List<Tag> tags = tagRepository.findAllByNomeContainingIgnoreCaseAndActiveTrue(containsSearch);

        if (tags == null) {
            LOGGER.info("Tag é nulo");
            return null;
        }

        return tags;
    }

    public List<Tag> findAll() {
        LOGGER.info("Find all tags");
        List<Tag> list = tagRepository.findAll();
        if (list == null) {
            LOGGER.info("Tag é nulo");
            return null;
        }
        return list;
    }

    public Tag find(Long id) {
        LOGGER.info("Find tag - {}", id);
        Tag model = tagRepository.findOneByIdAndActiveTrue(id);
        if (model == null) {
            LOGGER.info("Tag {} not found", id);
            return null;
        }
        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public Tag create(Tag model) {
        LOGGER.info("Creating Glossary");
        tagRepository.save(model);
        LOGGER.debug("Created: {}", model);
        return model;
    }

    public Tag update(Tag original, Tag current) {
        LOGGER.info("Updating tag - {}", original.getId());
        merge(original, current);
        tagRepository.save(original);
        LOGGER.debug("Updated: {}", original);
        return original;
    }

    public Tag delete(Tag model) {
        LOGGER.info("Removing Tag - {}", model.getId());
        model.setActive(false);
        tagRepository.save(model);
        LOGGER.debug("Tag: {}", model);
        return model;
    }

    private void merge(Tag original, Tag current) {
        if (!StringUtils.isEmpty(current.getNome())) {
            original.setNome(current.getNome());
        }

        if (!StringUtils.isEmpty(current.getCategoria())) {
            original.setCategoria(current.getCategoria());
        }

//        if (!StringUtils.isEmpty(current.getPosicionamento())) {
//            original.setPosicionamento(current.getPosicionamento());
//        }
    }

    public List<Tag> findTagsFromCategory(Long id) {
        LOGGER.info("Find tags from given category id: " + id);
        List<Tag> list = tagRepository.getTagsFromCategory(id);
        if (list == null) {
            LOGGER.info("Não existe tag com essa categoria");
            return null;
        }
        return list;
    }

    /**
     * Método para normalizar string para pesquisa com LIKE
     * @param search String para pesquisa
     * @return String normalizada
     */
    private String normalizeForContainsSearch(String search) {
        return Normalizer.normalize(search, Normalizer.Form.NFD) // acentuação
            .trim() // espaçamento
            .replaceAll("[^\\p{ASCII}]", "") // caracteres especiais
            .replaceAll(" +", " ") // múltiplos espaços
            .replaceAll(" ", "%"); // espaços -> % (para complementar buscar com LIKE)
    }
}
