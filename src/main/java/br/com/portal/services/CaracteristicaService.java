package br.com.portal.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.portal.entities.shared.Caracteristica;
import br.com.portal.repositories.CaracteristicaRepository;

import java.util.List;

@Service
public class CaracteristicaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaracteristicaService.class);

    @Autowired
    private CaracteristicaRepository caracteristicaRepository;

    public List<Caracteristica> findAll() {
        LOGGER.info("Find all Caracteristica");
        List<Caracteristica> list = caracteristicaRepository.findAll();

        if (list == null) {
            LOGGER.info("Caracteristica é nulo");
            return null;
        }

        return list;
    }

    public Caracteristica find(Long id) {
        LOGGER.info("Find Caracteristica - {}", id);
        Caracteristica model = caracteristicaRepository.findOne(id);

        if (model == null) {
            LOGGER.info("Caracteristica {} not found", id);
            return null;
        }

        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public Caracteristica create(Caracteristica model) {
        LOGGER.info("Creating Caracteristica");
        caracteristicaRepository.save(model);
        LOGGER.debug("Created: {}", model);
        return model;
    }

//    public Filtro update(Filtro original, Filtro current) {
//        LOGGER.info("Updating Filtro - {}", original.getId());
//        merge(original, current);
//        filtroRepository.save(original);
//        LOGGER.debug("Updated: {}", original);
//        return original;
//    }

    public Caracteristica delete(Caracteristica model) {
        LOGGER.info("Removing Caracteristica - {}", model.getId());
        caracteristicaRepository.delete(model);
        LOGGER.debug("Caracteristica: {}", model);
        return model;
    }

//    private void merge(Filtro original, Filtro current) {
//        if (!StringUtils.isEmpty(current.getNome())) {
//            original.setNome(current.getNome());
//        }
//
//        if (!StringUtils.isEmpty(current.getCategoria())) {
//            original.setCategoria(current.getCategoria());
//        }
//
//        if(!StringUtils.isEmpty(current.getPosicionamento())){
//            original.setPosicionamento(current.getPosicionamento());
//        }
//    }

}
