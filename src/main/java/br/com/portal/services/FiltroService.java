package br.com.portal.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.portal.entities.shared.Filtro;
import br.com.portal.repositories.FiltroRepository;

import java.util.List;

@Service
public class FiltroService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FiltroService.class);

    @Autowired
    private FiltroRepository filtroRepository;

    public List<Filtro> findAll() {
        LOGGER.info("Find all filtros");
        List<Filtro> list = filtroRepository.findAll();
        if (list == null) {
            LOGGER.info("Filtro é nulo");
            return null;
        }
        return list;
    }

    public Filtro find(Long id) {
        LOGGER.info("Find tag - {}", id);
        Filtro model = filtroRepository.findOne(id);
        if (model == null) {
            LOGGER.info("Tag {} not found", id);
            return null;
        }
        LOGGER.debug("Retrieved: {}", model);
        return model;
    }

    public Filtro create(Filtro model) {
        LOGGER.info("Creating Filtro");
        filtroRepository.save(model);
        LOGGER.debug("Created: {}", model);
        return model;
    }

//    public Filtro update(Filtro original, Filtro current) {
//        LOGGER.info("Updating Filtro - {}", original.getId());
//        merge(original, current);
//        filtroRepository.save(original);
//        LOGGER.debug("Updated: {}", original);
//        return original;
//    }

    public Filtro delete(Filtro model) {
        LOGGER.info("Removing Filtro - {}", model.getId());
        filtroRepository.delete(model);
        LOGGER.debug("Filtro: {}", model);
        return model;
    }

//    private void merge(Filtro original, Filtro current) {
//        if (!StringUtils.isEmpty(current.getNome())) {
//            original.setNome(current.getNome());
//        }
//
//        if (!StringUtils.isEmpty(current.getCategoria())) {
//            original.setCategoria(current.getCategoria());
//        }
//
//        if(!StringUtils.isEmpty(current.getPosicionamento())){
//        	original.setPosicionamento(current.getPosicionamento());
//        }
//    }

}
