package br.com.portal.data;

public enum Role {
    ADMINISTRATOR,
    INSTITUTION,
    ENTREPRENEUR,
    GUEST
}
