package br.com.portal.data;

public enum PrincipalUsuario {
    ESTUDANTE,
    PROFESSOR,
    DIRETOR
}
