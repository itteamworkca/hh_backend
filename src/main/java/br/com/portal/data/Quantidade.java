package br.com.portal.data;

public enum Quantidade {
    ATE_10,
    ATE_100,
    ATE_1000
}
