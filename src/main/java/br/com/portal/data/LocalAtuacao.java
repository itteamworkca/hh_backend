package br.com.portal.data;

public enum LocalAtuacao {
    MUNICIPIO,
    ESTADO,
    REGIAO,
    PAIS,
    MUNDO
}
