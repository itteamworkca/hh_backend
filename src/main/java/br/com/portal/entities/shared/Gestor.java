package br.com.portal.entities.shared;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import br.com.portal.entities.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class Gestor extends AbstractEntity {

    private Long id;
    private String nome;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    @Length(max = 255)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if (StringUtils.hasText(nome)) {
            nome = nome.trim();
        }

        this.nome = nome;
    }
}
