package br.com.portal.entities.shared;

import org.hibernate.annotations.GenericGenerator;

import br.com.portal.entities.AbstractEntity;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class User extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    private UUID id;
    private Login login;
    private String areaAtuacao;
    private String empresa;
    private String especialidade;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Valid
    @NotNull
    @Embedded
    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    @Override
    @PrePersist
    protected void onCreate() {
        super.onCreate();
        setId(UUID.randomUUID());
    }

    public String getAreaAtuacao() {
        return areaAtuacao;
    }

    public void setAreaAtuacao(String areaAtuacao) {
        this.areaAtuacao = areaAtuacao;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }
}
