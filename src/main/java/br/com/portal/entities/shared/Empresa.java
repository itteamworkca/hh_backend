package br.com.portal.entities.shared;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.portal.data.LocalAtuacao;
import br.com.portal.entities.AbstractEntity;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Empresa extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String nome;
    private String logo;
    private String telefone;
    private String email;
    private String site;
    private Date dataFundacao;
    private Long responsavelId;
    private LocalAtuacao localAtuacao;
    private Endereco endereco;
    private String descricao;
    private boolean vendeuParaGoverno;
    private Set<PrincipalCliente> principaisClientes;
    private String faixaFaturamento;
    private Integer funcionariosTempoIntegral;
    private Set<Gestor> gestores;
    private boolean aceleracaoOuIncubadora;
    private double valorInvestimento;
    private String tipoInvestimento;
    private String nomeAceleradora;
    private boolean buscaInvestimento;
    private Set<Solucao> solucoes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @NotBlank
    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @NotBlank
    @Email
    @Column(nullable = false, length = 255)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(length = 255)
    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @JsonFormat(pattern = "dd/MM/yyyy")
    public Date getDataFundacao() {
        return dataFundacao;
    }

    public void setDataFundacao(Date dataFundacao) {
        this.dataFundacao = dataFundacao;
    }

    public Long getResponsavelId() {
        return responsavelId;
    }

    public void setResponsavelId(Long responsavelId) {
        this.responsavelId = responsavelId;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    public LocalAtuacao getLocalAtuacao() {
        return localAtuacao;
    }

    public void setLocalAtuacao(LocalAtuacao localAtuacao) {
        this.localAtuacao = localAtuacao;
    }

    @Valid
    @NotNull
    @Embedded
    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    @NotBlank
    @Length(max = 500)
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isVendeuParaGoverno() {
        return vendeuParaGoverno;
    }

    public void setVendeuParaGoverno(boolean vendeuParaGoverno) {
        this.vendeuParaGoverno = vendeuParaGoverno;
    }

    @NotNull
    @OneToMany(cascade = CascadeType.ALL)
    public Set<PrincipalCliente> getPrincipaisClientes() {
        if (principaisClientes == null) {
            principaisClientes = new HashSet<>();
        }

        return principaisClientes;
    }

    public void setPrincipaisClientes(Set<PrincipalCliente> principaisClientes) {
        this.principaisClientes = principaisClientes;
    }

    //	@NotBlank
    @Length(max = 60)
    public String getFaixaFaturamento() {
        return faixaFaturamento;
    }

    public void setFaixaFaturamento(String faixaFaturamento) {
        this.faixaFaturamento = faixaFaturamento;
    }

    @NotNull
    public Integer getFuncionariosTempoIntegral() {
        return funcionariosTempoIntegral;
    }

    public void setFuncionariosTempoIntegral(Integer funcionariosTempoIntegral) {
        this.funcionariosTempoIntegral = funcionariosTempoIntegral;
    }

    @OneToMany(cascade = CascadeType.ALL)
    public Set<Gestor> getGestores() {
        if (gestores == null) {
            gestores = new HashSet<>();
        }

        return gestores;
    }

    public void setGestores(Set<Gestor> gestores) {
        this.gestores = gestores;
    }

    public boolean isAceleracaoOuIncubadora() {
        return aceleracaoOuIncubadora;
    }

    public void setAceleracaoOuIncubadora(boolean aceleracaoOuIncubadora) {
        this.aceleracaoOuIncubadora = aceleracaoOuIncubadora;
    }

    public double getValorInvestimento() {
        return valorInvestimento;
    }

    public void setValorInvestimento(double valorInvestimento) {
        this.valorInvestimento = valorInvestimento;
    }

    public String getTipoInvestimento() {
        return tipoInvestimento;
    }

    public void setTipoInvestimento(String tipoInvestimento) {
        this.tipoInvestimento = tipoInvestimento;
    }

    @Length(max = 100)
    public String getNomeAceleradora() {
        return nomeAceleradora;
    }

    public void setNomeAceleradora(String nomeAceleradora) {
        this.nomeAceleradora = nomeAceleradora;
    }

    public boolean isBuscaInvestimento() {
        return buscaInvestimento;
    }

    public void setBuscaInvestimento(boolean buscaInvestimento) {
        this.buscaInvestimento = buscaInvestimento;
    }

    @OneToMany(cascade = CascadeType.ALL)
    public Set<Solucao> getSolucoes() {
        if (solucoes == null) {
            solucoes = new HashSet<>();
        }

        return solucoes;
    }

    public void setSolucoes(Set<Solucao> solucoes) {
        this.solucoes = solucoes;
    }


}
