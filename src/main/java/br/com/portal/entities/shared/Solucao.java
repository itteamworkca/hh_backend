package br.com.portal.entities.shared;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.portal.data.ParaQuem;
import br.com.portal.data.PrincipalUsuario;
import br.com.portal.data.Quantidade;
import br.com.portal.data.RedePiloto;
import br.com.portal.entities.AbstractEntity;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Solucao extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String nome;
    private String site;
    private String resumo;
    private String beneficiosAEscola;
    private String beneficiosAoGestor;
    private String beneficiosAoAluno;
    private String beneficiosAoProfessor;
    private String pros;
    private String contras;
    private Set<SolucaoTag> solucaoTags;
    private Set<PrincipalCliente> principaisClientes;
    private PrincipalUsuario principalUsuario;
    private ParaQuem paraQuem;
    private Set<EstudoDeCaso> estudosDeCaso;
    private Set<Caracteristica> caracteristicas;
    private Set<Imagem> imagens;
    private Date dataCadastro;
    private Date dataAtualizacao;
    private boolean realizouPiloto;
    private RedePiloto redePiloto;
    private double tempoPiloto;
    private int quantidadePiloto;
    private String perfilCliente;
    private Quantidade quantidadeClientes;
    private Quantidade quantidadeUsuarios;
    private boolean testouSolucao;
    private Set<Comentario> comentarios;
    private Set<Avaliacao> avaliacoes;
    private Empresa empresa;

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public String getBeneficiosAEscola() {
        return beneficiosAEscola;
    }

    public void setBeneficiosAEscola(String beneficiosAEscola) {
        this.beneficiosAEscola = beneficiosAEscola;
    }

    public String getBeneficiosAoGestor() {
        return beneficiosAoGestor;
    }

    public void setBeneficiosAoGestor(String beneficiosAoGestor) {
        this.beneficiosAoGestor = beneficiosAoGestor;
    }

    public String getBeneficiosAoAluno() {
        return beneficiosAoAluno;
    }

    public void setBeneficiosAoAluno(String beneficiosAoAluno) {
        this.beneficiosAoAluno = beneficiosAoAluno;
    }

    public String getBeneficiosAoProfessor() {
        return beneficiosAoProfessor;
    }

    public void setBeneficiosAoProfessor(String beneficiosAoProfessor) {
        this.beneficiosAoProfessor = beneficiosAoProfessor;
    }

    public String getPros() {
        return pros;
    }

    public void setPros(String pros) {
        this.pros = pros;
    }

    public String getContras() {
        return contras;
    }

    public void setContras(String contras) {
        this.contras = contras;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.solucao", cascade = CascadeType.ALL)
    public Set<SolucaoTag> getSolucaoTags() {
        if (solucaoTags == null) {
            solucaoTags = new HashSet<>();
        }

        return solucaoTags;
    }

    public void setSolucaoTags(Set<SolucaoTag> solucaoTags) {
        this.solucaoTags = solucaoTags;
    }

    @OneToMany(cascade = CascadeType.ALL)
    public Set<PrincipalCliente> getPrincipaisClientes() {
        if (principaisClientes == null) {
            principaisClientes = new HashSet<>();
        }

        return principaisClientes;
    }

    public void setPrincipaisClientes(Set<PrincipalCliente> principaisClientes) {
        this.principaisClientes = principaisClientes;
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    public PrincipalUsuario getPrincipalUsuario() {
        return principalUsuario;
    }

    public void setPrincipalUsuario(PrincipalUsuario principalUsuario) {
        this.principalUsuario = principalUsuario;
    }

    @Enumerated(EnumType.STRING)
    public ParaQuem getParaQuem() {
        return paraQuem;
    }

    public void setParaQuem(ParaQuem paraQuem) {
        this.paraQuem = paraQuem;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public boolean isRealizouPiloto() {
        return realizouPiloto;
    }

    public void setRealizouPiloto(boolean realizouPiloto) {
        this.realizouPiloto = realizouPiloto;
    }

    @Enumerated(EnumType.STRING)
    public RedePiloto getRedePiloto() {
        return redePiloto;
    }

    public void setRedePiloto(RedePiloto redePiloto) {
        this.redePiloto = redePiloto;
    }

    public double getTempoPiloto() {
        return tempoPiloto;
    }

    public void setTempoPiloto(double tempoPiloto) {
        this.tempoPiloto = tempoPiloto;
    }

    public int getQuantidadePiloto() {
        return quantidadePiloto;
    }

    public void setQuantidadePiloto(int quantidadePiloto) {
        this.quantidadePiloto = quantidadePiloto;
    }

    public String getPerfilCliente() {
        return perfilCliente;
    }

    public void setPerfilCliente(String perfilCliente) {
        this.perfilCliente = perfilCliente;
    }

    @Enumerated(EnumType.STRING)
    public Quantidade getQuantidadeClientes() {
        return quantidadeClientes;
    }

    public void setQuantidadeClientes(Quantidade quantidadeClientes) {
        this.quantidadeClientes = quantidadeClientes;
    }

    @Enumerated(EnumType.STRING)
    public Quantidade getQuantidadeUsuarios() {
        return quantidadeUsuarios;
    }

    public void setQuantidadeUsuarios(Quantidade quantidadeUsuarios) {
        this.quantidadeUsuarios = quantidadeUsuarios;
    }

    public boolean isTestouSolucao() {
        return testouSolucao;
    }

    public void setTestouSolucao(boolean testouSolucao) {
        this.testouSolucao = testouSolucao;
    }

    @OneToMany(cascade = CascadeType.ALL)
    public Set<EstudoDeCaso> getEstudosDeCaso() {
        if (estudosDeCaso == null) {
            estudosDeCaso = new HashSet<>();
        }

        return estudosDeCaso;
    }

    public void setEstudosDeCaso(Set<EstudoDeCaso> estudosDeCaso) {
        this.estudosDeCaso = estudosDeCaso;
    }

    @OneToMany
    public Set<Imagem> getImagens() {
        if (imagens == null) {
            imagens = new HashSet<>();
        }

        return imagens;
    }

    public void setImagens(Set<Imagem> imagens) {
        this.imagens = imagens;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    public Set<Caracteristica> getCaracteristicas() {
        if (caracteristicas == null) {
            caracteristicas = new HashSet<>();
        }

        return caracteristicas;
    }

    public void setCaracteristicas(Set<Caracteristica> caracteristicas) {
        this.caracteristicas = caracteristicas;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "solucao")
	public Set<Comentario> getComentarios() {
    	if(comentarios == null){
    		comentarios = new HashSet<>();
    	}
		return comentarios;
	}

	public void setComentarios(Set<Comentario> comentarios) {
		this.comentarios = comentarios;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "solucao")
	public Set<Avaliacao> getAvaliacoes() {
		return avaliacoes;
	}

	public void setAvaliacoes(Set<Avaliacao> avaliacoes) {
		this.avaliacoes = avaliacoes;
	}

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

}
