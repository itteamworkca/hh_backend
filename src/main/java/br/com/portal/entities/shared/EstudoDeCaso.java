package br.com.portal.entities.shared;

import org.hibernate.validator.constraints.NotBlank;

import br.com.portal.entities.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
public class EstudoDeCaso extends AbstractEntity  {

    private Long id;
    private String nome;
    private Set<Imagem> imagens;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @NotNull
    @OneToMany
    public Set<Imagem> getImagens() {
        return imagens;
    }

    public void setImagens(Set<Imagem> imagens) {
        this.imagens = imagens;
    }


}
