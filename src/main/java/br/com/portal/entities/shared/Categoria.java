package br.com.portal.entities.shared;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.portal.entities.AbstractEntity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
public class Categoria extends AbstractEntity {

    private Long id;
    private String nome;
    private Instituicao instituicao;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @JsonIgnore
    @ManyToOne
    public Instituicao getInstituicao() {
        return instituicao;
    }

    public void setInstituicao(Instituicao instituicao) {
        this.instituicao = instituicao;
    }
}
