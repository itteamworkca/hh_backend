package br.com.portal.entities.shared;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.portal.entities.AbstractEntity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Caracteristica extends AbstractEntity {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String nome;
    private Filtro filtro;
    private Set<Solucao> solucoes;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @NotNull
    @ManyToOne
    @JsonIgnore
    public Filtro getFiltro() {
        return filtro;
    }

    public void setFiltro(Filtro filtro) {
        this.filtro = filtro;
    }

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "caracteristicas")
    public Set<Solucao> getSolucoes() {
        return solucoes;
    }

    public void setSolucoes(Set<Solucao> solucoes) {
        this.solucoes = solucoes;
    }
}
