package br.com.portal.entities.shared;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Endereco implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cep;
    private String endereco;
    private String complemento;
    private String numero;
    private String bairro;
    private String cidade;
    private String estado;
    private String pais;

    @NotBlank
    @Length(max = 9)
    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        if (StringUtils.hasText(cep)) {
            cep = cep.trim();
        }

        this.cep = cep;
    }

    @NotBlank
    @Length(max = 128)
    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        if (StringUtils.hasText(endereco)) {
            endereco = endereco.trim();
        }

        this.endereco = endereco;
    }

    @Length(max = 60)
    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        if (StringUtils.hasText(complemento)) {
            complemento = complemento.trim();
        }
        this.complemento = complemento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        if (StringUtils.hasText(numero)) {
            numero = numero.trim();
        }
        this.numero = numero;
    }

    @NotBlank
    @Length(max = 60)
    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        if (StringUtils.hasText(cidade)) {
            cidade = cidade.trim();
        }

        this.cidade = cidade;
    }

    @NotBlank
    @Length(min = 2, max = 2)
    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        if (StringUtils.hasText(estado)) {
            estado = estado.trim();
        }
        this.estado = estado;
    }

    @Length(max = 30)
    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        if (StringUtils.hasText(bairro)) {
            bairro = bairro.trim();
        }
        this.bairro = bairro;
    }

    @Length(max = 20)
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        if (StringUtils.hasText(pais)) {
            pais = pais.trim();
        }
        this.pais = pais;
    }
}
