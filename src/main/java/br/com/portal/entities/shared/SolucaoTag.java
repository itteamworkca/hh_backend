package br.com.portal.entities.shared;

import org.hibernate.validator.constraints.NotBlank;

import br.com.portal.entities.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@AssociationOverrides(value = {
    @AssociationOverride(name = "pk.solucao", joinColumns = @JoinColumn(name = "solucao_id")),
    @AssociationOverride(name = "pk.tag", joinColumns = @JoinColumn(name = "tag_id"))
})
public class SolucaoTag extends AbstractEntity {

    private SolucaoTagId pk = new SolucaoTagId();
    private String descricao;
    private int posicionamento;

    @EmbeddedId
    public SolucaoTagId getPk() {
        return pk;
    }

    public void setPk(SolucaoTagId pk) {
        this.pk = pk;
    }

    @NotBlank
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @NotNull
    public int getPosicionamento() {
        return posicionamento;
    }

    public void setPosicionamento(int posicionamento) {
        this.posicionamento = posicionamento;
    }

    @Transient
    public Solucao getSolucao() {
        return getPk().getSolucao();
    }

    public void setSolucao(Solucao solucao) {
        getPk().setSolucao(solucao);
    }

    @Transient
    public Tag getTag() {
        return getPk().getTag();
    }

    public void setTag(Tag tag) {
        getPk().setTag(tag);
    }

    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        SolucaoTag that = (SolucaoTag) o;

        if (getPk() != null ? !getPk().equals(that.getPk())
            : that.getPk() != null)
            return false;

        return true;
    }

    public int hashCode() {
        return (getPk() != null ? getPk().hashCode() : 0);
    }
}
