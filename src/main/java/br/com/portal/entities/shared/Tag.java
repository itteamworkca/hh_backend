package br.com.portal.entities.shared;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.portal.entities.AbstractEntity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Tag extends AbstractEntity {

    private Long id;
    private String nome;
    private Categoria categoria;
    private Set<SolucaoTag> solucaoTags;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @NotNull
    @ManyToOne
    @JsonIgnore
    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.tag")
    @JsonIgnore
    public Set<SolucaoTag> getSolucaoTags() {
        if (solucaoTags == null) {
            solucaoTags = new HashSet<>();
        }

        return solucaoTags;
    }

    public void setSolucaoTags(Set<SolucaoTag> solucaoTags) {
        this.solucaoTags = solucaoTags;
    }
}
