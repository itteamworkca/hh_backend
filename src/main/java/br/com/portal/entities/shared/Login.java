package br.com.portal.entities.shared;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.portal.data.Role;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Embeddable
public class Login implements Serializable {

    private static final long serialVersionUID = 1L;

    private String email;
    private String password;
    private String salt;
    private Boolean accountEnabled;
    private Boolean accountNonExpired;
    private Boolean credentialsNonExpired;
    private Boolean accountNonLocked;
    private List<Role> roles;

    @NotBlank
    @Column(nullable = false, length = 255)
    @Email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (StringUtils.hasText(email)) {
            email = email.toLowerCase();
        }

        this.email = email;
    }

    @NotBlank
    @Column(nullable = false, length = 60)
    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }


    @Column(nullable = false, length = 29)
    @JsonIgnore
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @JsonIgnore
    public Boolean getAccountEnabled() {
        return accountEnabled;
    }

    public void setAccountEnabled(Boolean accountEnabled) {
        this.accountEnabled = accountEnabled;
    }

    @JsonIgnore
    public Boolean getAccountNonExpired() {
        return accountNonExpired;
    }

    public void setAccountNonExpired(Boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }

    @JsonIgnore
    public Boolean getCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }

    @JsonIgnore
    public Boolean getAccountNonLocked() {
        return accountNonLocked;
    }

    public void setAccountNonLocked(Boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @ElementCollection
    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    @JsonIgnore
    public List<Role> getRoles() {
        if (roles == null) {
            roles = new ArrayList<>();
        }

        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}
