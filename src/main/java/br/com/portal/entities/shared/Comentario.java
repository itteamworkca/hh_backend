package br.com.portal.entities.shared;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.portal.entities.AbstractEntity;

@Entity
//@JsonSerialize(using = ComentarioSerializer.class)
public class Comentario extends AbstractEntity implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long id;
    private String comentario;
    
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    private Solucao solucao;

    @NotBlank
    @Length(max = 255)
    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        if (StringUtils.hasText(comentario)) {
        	comentario = comentario.trim();
        }

        this.comentario = comentario;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonBackReference
	public Solucao getSolucao() {
		if(solucao == null){
			solucao = new Solucao();
		}
		return solucao;
	}

	public void setSolucao(Solucao solucao) {
		this.solucao = solucao;
	}
}
