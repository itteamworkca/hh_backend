package br.com.portal.entities.shared;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class SolucaoTagId implements Serializable {

    private Solucao solucao;
    private Tag tag;

    @ManyToOne
    public Solucao getSolucao() {
        return solucao;
    }

    public void setSolucao(Solucao solucao) {
        this.solucao = solucao;
    }

    @ManyToOne
    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SolucaoTagId that = (SolucaoTagId) o;

        if (solucao != null ? !solucao.equals(that.solucao) : that.solucao != null) return false;
        if (tag != null ? !tag.equals(that.tag) : that.tag != null)
            return false;

        return true;
    }

    public int hashCode() {
        int result;
        result = (solucao != null ? solucao.hashCode() : 0);
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        return result;
    }
}
