package br.com.portal.entities.shared;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.portal.entities.AbstractEntity;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class PrincipalCliente extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String nome;
    private Date desde;
    private String volume;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    @Length(max = 255)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if (StringUtils.hasText(nome)) {
            nome = nome.trim();
        }

        this.nome = nome;
    }

    @NotBlank
    @Length(max = 128)
    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        if (StringUtils.hasText(volume)) {
            volume = volume.trim();
        }

        this.volume = volume;
    }

    @JsonFormat(pattern = "dd/MM/yyyy")
    public Date getDesde() {
        return desde;
    }

    public void setDesde(Date desde) {
        this.desde = desde;
    }


}
