package br.com.portal.entities.shared;

import java.sql.Date;
import java.util.List;

public class ResultadosBusca {
	
	private Long id;
	private String titulo;
	private String subtitulo;
	private String descricao;
	private String imagem;
	private List<Tag> tags;
	private Date data;
	private String empresa;
	private String principalUsuario;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getSubtitulo() {
		return subtitulo;
	}
	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public List<Tag> getTags() {
		return tags;
	}
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getPrincipalUsuario() {
		return principalUsuario;
	}
	public void setPrincipalUsuario(String principalUsuario) {
		this.principalUsuario = principalUsuario;
	}
	
	

}
