package br.com.portal.entities.shared;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import br.com.portal.entities.AbstractEntity;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class Glossario extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    private UUID id;
    private String titulo;
    private String descricao;
    private String imagem;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "BINARY(16)")
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @NotBlank
    @Length(max = 80)
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @NotBlank
    @Type(type = "text")
    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    @Override
    @PrePersist
    protected void onCreate() {
        super.onCreate();
        setId(UUID.randomUUID());
    }
}
