package br.com.portal.entities.shared;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import br.com.portal.data.Role;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@PrimaryKeyJoinColumn
public class Instituicao extends User {

    private static final long serialVersionUID = 1L;

    private String nome;
    private Endereco endereco;

    @NotBlank
    @Length(max = 60)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if (StringUtils.hasText(nome)) {
            nome = nome.trim();
        }
        this.nome = nome;
    }

    @Valid
    @NotNull
    @Embedded
    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    @Override
    @PrePersist
    protected void onCreate() {
        super.onCreate();
        getLogin().getRoles().add(Role.INSTITUTION);
        getLogin().setAccountEnabled(true);
        getLogin().setAccountNonExpired(true);
        getLogin().setCredentialsNonExpired(true);
        getLogin().setAccountNonLocked(true);
    }
}
