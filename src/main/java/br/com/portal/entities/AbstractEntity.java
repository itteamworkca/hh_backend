package br.com.portal.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;

@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    private DateTime createdAt;
    private DateTime updatedAt;
    private Boolean active;

    @Column(nullable = false, updatable = false, columnDefinition = "datetime default CURRENT_TIMESTAMP")
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonIgnore
    public DateTime getCreatedAt() {
        return createdAt;
    }

    protected void setCreatedAt(DateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonIgnore
    public DateTime getUpdatedAt() {
        return updatedAt;
    }

    protected void setUpdatedAt(DateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Column(nullable = false, columnDefinition = "boolean default true")
    @JsonIgnore
    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @PrePersist
    protected void onCreate() {
        setCreatedAt(new DateTime());
        setActive(true);
    }

    @PreUpdate
    protected void onUpdate() {
        setUpdatedAt(new DateTime());
    }
}
