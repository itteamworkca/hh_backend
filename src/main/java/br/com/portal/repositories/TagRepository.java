package br.com.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.Tag;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findOneByIdAndActiveTrue(Long id);

    List<Tag> findAllByNomeContainingIgnoreCaseAndActiveTrue(String search);

    List<Tag> findAllByCategoriaAndActiveTrue(Long id);

    @Query(value = "SELECT * from tag t where t.categoria_id = ?1", nativeQuery = true)
    List<Tag> getTagsFromCategory(Long id);


}
