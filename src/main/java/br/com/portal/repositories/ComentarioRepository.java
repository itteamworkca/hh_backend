package br.com.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.Comentario;

@Repository
public interface ComentarioRepository extends JpaRepository<Comentario, Long> {


}
