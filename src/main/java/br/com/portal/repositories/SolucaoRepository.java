package br.com.portal.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.Solucao;
import br.com.portal.entities.shared.Tag;

@Repository
public interface SolucaoRepository extends JpaRepository<Solucao, Long> {

	List<Solucao> findAllByActiveTrue();

	List<Solucao> findAllByActiveTrueAndNomeContaining(String search);

	@Query(value = "SELECT * FROM Solucao WHERE MATCH (nome, resumo) AGAINST (?1);", nativeQuery = true)
	List<Solucao> buscaFreeTextBySolucao(String search);
	
	@Query(value = "SELECT * FROM Tag WHERE MATCH (nome) AGAINST (?1);", nativeQuery = true)
	List<Tag> buscaByTagFreeText(String search);

	@Query(value = "select * from Solucao s JOIN SolucaoTag st ON (s.id = st.solucao_id) where MATCH (st.descricao) AGAINST (?1);", nativeQuery = true)
	List<Solucao> buscaSolucaoByTagDescricaoFreeText(String search);
}
