package br.com.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.Empresa;

import java.util.List;
import java.util.UUID;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, UUID> {

    Empresa findOneByIdAndActiveTrue(Long id);

    List<Empresa> findAllByActiveTrue();

}
