package br.com.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.Instituicao;

import java.util.List;
import java.util.UUID;

@Repository
public interface InstituicaoRepository extends JpaRepository<Instituicao, UUID> {
    List<Instituicao> findAllByLoginEmailAndActiveTrue(String email);

    List<Instituicao> findAllByLoginEmailAndIdNotAndActiveTrue(String email, UUID id);

    Instituicao findOneByIdAndActiveTrue(UUID id);
}
