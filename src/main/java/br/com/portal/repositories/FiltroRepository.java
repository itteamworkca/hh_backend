package br.com.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.Filtro;

@Repository
public interface FiltroRepository extends JpaRepository<Filtro, Long> {


}
