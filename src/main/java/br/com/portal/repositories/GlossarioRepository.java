package br.com.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.Glossario;

import java.util.List;
import java.util.UUID;

@Repository
public interface GlossarioRepository extends JpaRepository<Glossario, UUID> {
    Glossario findOneByIdAndActiveTrue(UUID id);

    List<Glossario> findAllByActiveTrue();

}
