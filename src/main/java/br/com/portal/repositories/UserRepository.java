package br.com.portal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.portal.entities.shared.User;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    User findOneByLoginEmailAndLoginAccountEnabledTrueAndActiveTrue(String email);

    User findOneByIdAndActiveTrue(UUID id);
}
