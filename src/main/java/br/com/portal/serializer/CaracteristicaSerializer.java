package br.com.portal.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import br.com.portal.entities.shared.Caracteristica;
import br.com.portal.services.CaracteristicaService;

import java.io.IOException;

public class CaracteristicaSerializer extends JsonSerializer<Caracteristica> {

    @Override
    public void serialize(Caracteristica value, JsonGenerator gen, SerializerProvider serializers)
        throws IOException, JsonProcessingException {

        CaracteristicaService cService = new CaracteristicaService();
        gen.writeStartObject();
        Caracteristica c;
        if (value.getId() != null) {
            c = cService.find(value.getId());
            if (c != null) {
                gen.writeNumberField("id", c.getId());
                gen.writeStringField("nome", c.getNome());
//                gen.writeObjectField("filtro", c.getFiltro());
            }

        }
//		if(value.getFiltro() != null){
//			gen.writeStartObject("filtro");
//			Filtro f = new Filtro();
//			f = fService.find(value.getFiltro().getId());
//			gen.writeObjectField("filtro", f);
//			gen.writeEndObject();
//		}
        gen.writeEndObject();
    }

}
