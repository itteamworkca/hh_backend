package br.com.portal.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class SecurityComponent {

    public static final int LOG_ROUNDS = 12;
    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityComponent.class);

    public Boolean checkPassword(String currentNotHashedPassword, String originalHashedPassword) {
        LOGGER.info("Checking password");

        if (StringUtils.isEmpty(originalHashedPassword) || StringUtils.isEmpty(currentNotHashedPassword)) {
            return false;
        }

        return BCrypt.checkpw(currentNotHashedPassword, originalHashedPassword);
    }

    public String[] hashPasswordWithBcrypt(String password, String salt) {
        LOGGER.info("Hashing password with bcrypt");
        LOGGER.info(" > Salt: {}", salt);

        LOGGER.debug("Plain password: {}", password);

        if (salt == null) {
            salt = BCrypt.gensalt(LOG_ROUNDS);
        }

        String hash = BCrypt.hashpw(password, salt);

        LOGGER.debug("Hashed password: {}", hash);

        return new String[]{hash, salt};
    }
}
