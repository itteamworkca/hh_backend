package br.com.portal.components;

import com.github.slugify.Slugify;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;

@Component
public class UploadComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadComponent.class);

    @Value("${spring.http.multipart.location}")
    private String path;

    public String store(MultipartFile file) {
        if (file.isEmpty()) {
            LOGGER.error("Image not found: {}", file);
            return null;
        }

        try {
            Path directory = Paths.get(path);
            String fileName = getStoreFileName(file);

            if (!Files.exists(directory)) {
                Files.createDirectories(directory);
            }

            Files.copy(file.getInputStream(), Paths.get(path, fileName));

            LOGGER.info("File saved: ", fileName);

            return fileName;
        } catch (IOException | RuntimeException | MimeTypeException e) {
            LOGGER.error(file.getOriginalFilename() + " => " + e.getMessage(), file.getOriginalFilename());
        }

        return null;
    }

    private String getStoreFileName(MultipartFile file) throws IOException, MimeTypeException {
        String originalFilename = file.getOriginalFilename();

        if (StringUtils.isEmpty(originalFilename)) {
            return null;
        }

        String basename = FilenameUtils.getBaseName(originalFilename);
        String extension = FilenameUtils.getExtension(originalFilename);

        if (StringUtils.isEmpty(extension)) {
            extension = getExtensionFromMimeType(file.getContentType());
        } else {
            extension = '.' + extension;
        }

        return slugify(getTimestamp(), basename, extension);
    }

    /**
     * Recuperar timestamp
     *
     * @return Horário
     */
    private String getTimestamp() {
        return String.valueOf(Calendar.getInstance().getTimeInMillis());
    }

    /**
     * Normalização do nome do arquivo
     *
     * @param timestamp
     * @param basename
     * @param extension
     * @return Nome do arquivo slugify com timestamp
     * @throws IOException
     */
    private String slugify(String timestamp, String basename, String extension) throws IOException {
        Slugify slg = new Slugify();
        return slg.slugify(basename + "-" + timestamp) + extension.toLowerCase();
    }

    /**
     * Recuperar extensão do arquivo a partir do mimetype
     *
     * @param mimeType
     * @return
     * @throws MimeTypeException
     */
    private String getExtensionFromMimeType(String mimeType) throws MimeTypeException {
        MimeTypes allTypes = MimeTypes.getDefaultMimeTypes();
        return allTypes.forName(mimeType).getExtension();
    }
}
