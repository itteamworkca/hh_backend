FROM java:8

MAINTAINER Thiago Henrique Poiani <thpoiani@gmail.com>
WORKDIR /edutec
VOLUME /root/.gradle

COPY gradle/ /edutec/gradle/
COPY gradlew /edutec/gradlew
COPY build.gradle /edutec/build.gradle
COPY src/ /edutec/src/

COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["java","-jar","/edutec/build/libs/edutec-0.0.1-SNAPSHOT.jar", "-Duser.timezone=UTC"]
